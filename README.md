Merbocop: Merge-Request Bot Police 🤖
=====================================

Build
-----

Get dependencies:

    opam switch create . 4.12.1  # Or provide a 4.12.1 OPAMSWITCH
    ./please.sh get_deps

and build:

    ./please.sh build

CI Setup
--------

We use Gitlab's “Pipeline Schedules:”
<https://gitlab.com/oxheadalpha/merbocop/pipeline_schedules>.


### Edit-CI

The directory `src/edit-ci/` contains a generator for `.gitlab-ci.yml` and
`README.md` files to be added to various branches for the schedule setup.

See:

    dune exec src/edit-ci/main.exe {show,diff,ensure,ensure --push}

The most useful command is `diff`, it shows what is left to do, and/or
discrepancies with setup currently live.

Then `ensure` creates/updates the branches but does not try to touch the
Schedules setup, you have to do what `diff` tells you manually (usually: create
schedules, set their “cron” configuration, and add “secret” variables).

Global configuration of `edit-ci` is done via environment variables:

- `edit_ci_remote`: name of the git-remote to _compare-with_ or _push-to_.
- `edit_ci_token`: GitLab API token (20 characters) of a user with enough access
  rights to query the configuration of the Schedules.
- `testing_edit_ci`: Enable some extra fake data to test `edit-ci` itself.

Example:

```sh
export edit_ci_remote=origin
export edit_ci_token=MyToken0123456789XYZ
dune exec src/edit-ci/main.exe -- diff
```

### Current Status

The 4 “testing” and 3 “production” schedules are active (cf. output of `dune
exec src/edit-ci/main.exe -- diff`), the older one is still there but:

- Branch ci-cron-reports is there
    - File "README.md" is ready.
    - File ".gitlab-ci.yml" is ready.
    - Existing schedule uses the right cron: "0 */3 * * *".
    - Existing schedule defines all the required variables ([] ⊂ []).
- Branch ci-cron-test-comments is there
    - File "README.md" is ready.
    - File ".gitlab-ci.yml" is ready.
    - Existing schedule uses the right cron: "0 */3 * * *".
    - Existing schedule defines all the required variables (["smbot_token"]
      ⊂ ["smbot_token"]).
- Branch ci-cron-test-labeling is there
    - File "README.md" is ready.
    - File ".gitlab-ci.yml" is ready.
    - Existing schedule uses the right cron: "0 */3 * * *".
    - Existing schedule defines all the required variables (["smbot_token"]
      ⊂ ["smbot_token"]).
- Branch ci-cron-test-approving is there
    - File "README.md" is ready.
    - File ".gitlab-ci.yml" is ready.
    - Existing schedule uses the right cron: "0 * * * *".
    - Existing schedule defines all the required variables (["smbot_token"]
      ⊂ ["smbot_token"]).
- Branch ci-cron-prod-comments is there
    - File "README.md" is ready.
    - File ".gitlab-ci.yml" is ready.
    - Existing schedule uses the right cron: "0 */6 * * *".
    - Existing schedule defines all the required variables
      (["tezbocop_token"] ⊂ ["tezbocop_token"]).
- Branch ci-cron-prod-labeling is there
    - File "README.md" is ready.
    - File ".gitlab-ci.yml" is ready.
    - Existing schedule uses the right cron: "0 */2 * * *".
    - Existing schedule defines all the required variables
      (["tezbocop_token"] ⊂ ["tezbocop_token"]).
- Branch ci-cron-prod-approving is there
    - File "README.md" is ready.
    - File ".gitlab-ci.yml" is ready.
    - Existing schedule uses the right cron: "0 * * * *".
    - Existing schedule defines all the required variables
      (["tezbocop_token"] ⊂ ["tezbocop_token"]).
- There is an orphan Schedule on branch "ci-schedule-configuration"
  (cron: "0 0,6,12,18 * * *", variables: ["full_cron_trigger"]).


### Old Setup

This section describes the way Merbocop was configured up to *Fri, 01 Oct 2021
10:00:00 -0400*. It is kept for now in case we need to quickly revert.

The schedule on `ci-schedule-configuration` is inactive.

Commit `906463a076568cf389cc9f3751d27cc67d41851b` removes its configuration in
the `.gitlab-ci.yml` file.

<blockquote>

A pipeline with `$full_cron_trigger == "true"` runs every 6 hours.

We get the CI configuration from the branch
[`ci-schedule-configuration`](https://gitlab.com/oxheadalpha/merbocop/commits/ci-schedule-configuration)
(it is used as a “moving pointer”).
See in the corresponding
[`.gitlab-ci.yml`](https://gitlab.com/oxheadalpha/merbocop/blob/ci-schedule-configuration/.gitlab-ci.yml) the jobs using the `<<: *full_cron_triggered` template.


But the `merbocop` code that actually runs comes from previously built docker
images, e.g.:

```yaml
  cron_image: "${CI_REGISTRY_IMAGE}:e408c5f1-run"
  cron_test_image: "${CI_REGISTRY_IMAGE}:ec28e7dc-run"
```

`cron_test_image` is used to run on an inspection on
[smondet/tezos](https://gitlab.com/smondet/tezos) and to make the “full report”
[page](https://smondet.gitlab.io/merbocop/full-tezos-tezos.html) on
[tezos/tezos](https://gitlab.com/tezos/tezos) (i.e. this version will not be
posting/editing comments on tezos/tezos).

`cron_image` is used to run the *real/production* inspection on
[tezos/tezos](https://gitlab.com/tezos/tezos) (the one that users see).


The `makewebsite` and `pages` jobs also run in the cron-pipeline, which is how
the reports are put on the website,
e.g. <https://smondet.gitlab.io/merbocop/full-tezos-tezos.html>.

</blockquote>

Use
---

To run Merbocop use the `full-inspect` argument, point Merbocop at a Gitlab project and provide the number of merger request that you would like Merbocop to inspect.

For example:
``` bash
$ dune exec ./merbocop -- full-inspect --whole-project-inspection [PROJECT-ID:LIMIT]
```

`whole-project-inspection` provides Merbocop with the Gitlab API `PROJECT-ID` and a `LIMIT` for the number of merge request to inspect. 

The bot user will require "Developer" status in order to label and approve merge requests. (See [Gitlab Permissions](https://docs.gitlab.com/ee/user/permissions.html)). It may also be necessary to include the bot user as an eligible approver depending on the projects approval rules (see [Gitlab approval rules](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html#merge-request-approval-rules)).

Options
-------

``` bash
--self-id [USER-ID] 
```

The gitlab user ID number for the bot user. Merbocop uses the bot user's `USER-ID` to post/edit comments and approve merge request.

``` bash
--commits-to-inspect [PROJECT:COMMIT]
```

Provide Merbocop with a specific Gitlab project ID and commit ID to inspect.


``` bash
--only-updated-in-the-past [NUM:days]
```

Restrict Merbocop to inspect merger requests updated in the last `NUM:days`.

``` bash
--dump-cache [PATH]
```

Dump the contents of the cache to `PATH`.

``` bash
--load-cache [PATH]
```

Load the contents of the cache from `PATH`.

``` bash
--write-body [PATH]
```

Write the body of Merbocop's message output to `PATH`. This produces a markdown file.

``` bash
--access-token [TOKEN]
```

Provide the Gitlab access `TOKEN` required for some of Merbocop's options. Be sure the access token has the appropriate scope for the given actions you would like Merbocop to preform. See the [Gitlab docs regarding access token scope](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#limiting-scopes-of-a-project-access-token).  

``` bash
--post-on-self
```

Post or edit comments on merger request. This requires the `--access-token` option.

``` bash
--no-comment-editing
```

Always add merger request comments without editing existing ones.

``` bash
--set-doc-label [LABEL]
```

Add a merger request label (`LABEL`) to all merge requests which make changes to **.rst** or **.md** files only. This option will also remove the label if it doesn't apply. This requires the `--access-token` option.

``` bash
--set-approved-label [LABEL]
```

Add a merger request label (`LABEL`) to all merge requests which have enough approvals to be merged. This will also remove the label if it doesn't apply. 
This requires the `--access-token` option.

``` bash
--approve-doc-only
```

Merbocop will approve merge requests which make changes to **.rst** or **.md** files only. This will also revoke approval form a previously approved merge requests, if it's no longer doc-only. This requires the `--access-toke` option. 

``` bash
--allow-failures
```

Will return 0 exit status for some errors.

``` bash
--http-retry-options [RETRY-LIMIT:WAIT:FACTOR]
```

Will set the parameters used to retry failed HTTP calls. Failed calls will be retried (`RETRY-LIMIT`) times. The first retry will be delayed by (`WAIT`) seconds. Subsequent retries will be increasingly delayed by (`FACTOR`). Default values are: retry-limit = 4, wait =  0.1,  factor = 2.

``` bash
--http-no-retry
```

Will not retry failed HTTP calls. 

``` bash
--post-warnings-to [PROJECT]:[ISSUE]:[DEBUGGING]
```

Will post merge reqeusts warnings as Gitlab "discussions" under an (`ISSUE`) in (`PROJECT`). A separate discussion will be created for each merge request. (`DEBUGGING`) can be "always" to post empty warnings or, "non-empty" to skip merge requests that have no warnings. 
