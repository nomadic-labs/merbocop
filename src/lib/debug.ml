open! Base

let dbg f = Fmt.kstr (Caml.Printf.eprintf "[merbocop-dbg:] %s\n%!") f

(** [expect] formates output for readable expect_tests. *)
let expect (s : string) : unit =
  s |> String.split_lines
  |> List.iter ~f:(fun l -> Fmt.pf Fmt.stdout "   %s\n%!" l)
